#Twitterbootstrap v3 guide
=============================

## Multilanguage data entry

### Set your default language

First of all you need to define the "default" language of your store. This is the language that is used when the customer hits www.domain.com/.
This language can be set [here](https://admin.solidshops.com/settings/general).

In the config partial of your theme you can change the following multilanguage variables:

* lang_multilanguage: you can set this variable to true if you want to enable multiple languages. This variable defaults to true.
* lang_multilanguage_list: contains the list of languages you want to use if lang_multilanguage is set to true
* lang_customfieldgroup: defines the name of the customfieldgroupname that contains the translated productfields. This can be any string you used during the customfieldgroup setup. In our case it's mostly "translations" or "Inhoud".

###Pages
If you want to show some "About us" information in the sidebar, an "About us" page should be created in the SolidShops backend.
Pages can be created [here](https://admin.solidshops.com/pages/list).

It's important that the name of the page you just created has the name "about-us" in the clean url. 
You can edit this value if you navigate to the details of your newly created page.

If it's a multilanguage store, multiple pages should be created where the clean url starts with the id of the language. For instance "nl-about-us" , "fr-about-us".

Each theme already contains the TOS and FAQ pages. The data of these pages can be found in a body_content_TOS_nl and body_content_faq_nl partial.

###Categories

Categories can be created while creating/updating products.

If it's a multiple language store, it's important the categories have the following structure:

+ nl
  * Een categorie
  * Een andere categorie
+ en
  * a category
  * Another category   	
  
###Custom fields

Custom fields can be used to add extra fields to admin's product screen. These values are automatically printed on the products page of your webshop.

If you are setting up a multiple language store, it's important to create a name and description field for each language.

For example: fr_name, fr_description

It's recommended to use a textfield for the name field and a wysiwyg for the description field.  




##Theme

### Change images
All the images for each theme can be changed with the SolidShops  assets manager.
Navigate to the detail of your theme and click on the "Manage theme assets" button. 

A popup will open and under the "img" folder you should change the banner.png and logo.png file.

### Update social buttons
At the top of the "config" partial, you can define an url for the facebook,twitter and linkedin buttons.
If a url is provided, the Social button will be shown in the "About Us" sidebar(if the page exists) and in the footer of the webshop.
The url's of all social media sites can be changed in the config.twig partial that is included in every template.

### CSS modifications
The theme contains a "css" folder with a theme.css file. This file can for instance be used to change the colors or layout in your theme.
If you are looking to make some changes to the checkout page a special checkout.css template can be used.
We recommend making the modifications in the sass file and make your sasscompiler generate the css file for you.

If you are looking for a sass compiler with a nice UI, you should take a look at [koala](http://koala-app.com/).






