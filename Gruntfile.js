module.exports = function(grunt) {

  grunt.initConfig({
	  pkg: grunt.file.readJSON('package.json'),
	  sass: {
	    dist: {
	    	files: [{
	            expand: true,
	            cwd: 'css/sass',
	            src: ['*.scss'],
	            dest: 'css',
	            ext: '.css.twig'
	          }]
	    }
	  },
	  jshint: {
		    theme: ['js/*.js.twig']
	   }
	});

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  
  grunt.registerTask('css', ['sass']);
  grunt.registerTask('js', ['jshint:theme']);


};